#include <stdlib.h>
#include <stdint.h>

#include "bool.h"

#ifndef I2C_H
#define I2C_H

typedef struct Buffer {
    size_t length;
    uint8_t* bytes;
} Buffer;

void i2c_init(int bitrate);

void i2c_writeByte(uint8_t address, uint8_t byte);
void i2c_writeBuffer(uint8_t address, Buffer buf);

uint8_t i2c_readByte(bool ack);
void i2c_readBuffer(Buffer buf);

#endif
