#ifndef BOOL_H
#define BOOL_H

typedef enum {
    false = 0x00,
    true = 0x01
} bool;

#define FALSE false
#define TRUE true

#endif
