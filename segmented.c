#include "segmented.h"
#include "i2c.h"

static const uint8_t glyphs[] = {
    0x3F, // 0
    0x06, // 1
    0x5B, // 2
    0x4F, // 3
    0x66, // 4
    0x6D, // 5
    0x7D, // 6
    0x07, // 7
    0x7F, // 8
    0x6F, // 9
    0x77, // A
    0x7C, // b
    0x58, // c
    0x5E, // d
    0x79, // E
    0x71, // F
    0x6F, // g
    0x74, // h (lowercase, leave "H" glyph for X)
    0x06, // I
    0x1E, // J
    0x78, // k
    0x38, // L
    0x37, // M (kind of a tall n)
    0x54, // n
    0x5C, // o
    0x73, // P
    0x67, // q
    0x50, // r
    0x6D, // S
    0x70, // t
    0x1C, // u
    0x0C, // v
    0x3E, // W (U) (get it?? DOUBLE U???)
    0x76, // X (H)
    0x6E, // y
    0x5B, // Z
    0x00, // " "
    0x40, // -
    0x08, // _
    0x48, // =
    0x53, // ?
    0x02, // '
    0x22, // "
    0x39, // (
    0x0F, // )
    0x63  // degree symbol 0xA7 ascii
};

inline size_t charPos(uint8_t pos) {
    switch (pos) {
        case 0: return 0;
        case 1: return 2;
        case 2: return 6;
        case 3: return 8;
    }

    return 10;
}

void disp_enable(Display* disp)
{
    i2c_writeByte(disp->address, 0x21);
}

void disp_disable(Display* disp)
{
    i2c_writeByte(disp->address, 0x20);
}

void disp_blank(Display* disp)
{
    // for (int i = 0; i < BUFFER_LEN; i++)
    //     disp->buffer[i] = 0x00;

    disp->buffer[0] = 0x06;
    disp->buffer[2] = 0x54;
    disp->buffer[6] = 0x06;
    disp->buffer[8] = 0x70;
}

void disp_setSettings(Display* disp, uint8_t dispSetting, uint8_t brightness)
{
    if (brightness > 15)
        brightness = 15;

    dispSetting &= 7;

    disp->settings = (dispSetting << 4) | brightness;
}

void disp_setGlyph(Display* disp, uint8_t position, uint8_t character)
{
    size_t pos = charPos(position);
    // preserve period
    disp->buffer[pos] = (disp->buffer[pos] & 0x80) | character;
}

bool disp_setDigit(Display* disp, uint8_t position, uint8_t value)
{
    if (position >= 4 || value > 16)
        return false;

    disp_setGlyph(disp, position, glyphs[value]);

    return true;
}

bool disp_setChar(Display* disp, uint8_t position, char value)
{
    if (position >= 4)
        return false;

    if (value >= '0' && value <= '9')
        disp_setGlyph(disp, position, glyphs[value - '0']);
    else if (value >= 'A' && value <= 'Z')
        disp_setGlyph(disp, position, glyphs[value - 'A' + 10]);
    else if (value >= 'a' && value <= 'z')
        disp_setGlyph(disp, position, glyphs[value - 'a' + 10]);
    else
    {
        size_t index = 0;
        // supported special chars
        switch(value)
        {
            case ' ': index = 36; break;
            case '-': index = 37; break;
            case '_': index = 38; break;
            case '=': index = 39; break;
            case '?': index = 40; break;
            case '\'': index = 41; break;
            case '"': index = 42; break;
            case '(': index = 43; break;
            case ')': index = 44; break;
        }

        if (index > 0)
            disp_setGlyph(disp, position, glyphs[index]);
        else
            return false;
    }

    return true;
}

bool disp_setPeriod(Display* disp, uint8_t position, bool on)
{
    if (position > 4)
        return false;

    disp->buffer[position * 2] |= (on << 7);

    return true;
}

void disp_setColon(Display* disp, bool on)
{
    if (on)
        disp->buffer[4] = 0x02;
    else
        disp->buffer[4] = 0x00;
}

bool disp_printUInt(Display* disp, uint16_t value, uint16_t base, bool isNegative)
{
    if ((!isNegative && value > base * base * base * base - 1)
         || (isNegative && value > base * base * base - 1))
        return false;

    if (isNegative)
        disp_setChar(disp, 0, '-');

    uint8_t digitVal;

    for (int place = 3; place >= (isNegative ? 1 : 0); place--)
    {
        digitVal = (uint8_t)(value % base);
        disp_setDigit(disp, place, digitVal);
        value /= base;
    }

    return true;
}

bool disp_printInt(Display* disp, int16_t value, uint16_t base)
{
    return disp_printUInt(disp, (uint16_t)(value < 0 ? -value : value), base, value < 0);
}

bool disp_printString(Display* disp, char* string)
{
    size_t i;

    for (i = 0; i < 4; i++)
    {
        if (string[i] == '\0')
            break;

        bool ok = disp_setChar(disp, i, string[i]);

        if (!ok)
            return false;
    }

    for (; i < 4; i++)
        disp_setChar(disp, i, ' ');

    return true;
}

void disp_update(Display* disp)
{
    Buffer b;
    b.length = BUFFER_LEN + 1;
    b.bytes = malloc(BUFFER_LEN + 1);

    b.bytes[0] = 0x00;

    for (size_t i = 0; i < BUFFER_LEN; i++)
        b.bytes[i+1] = disp->buffer[i];

    uint8_t brightness = 0xE0 | (disp->settings & 0x0F);

    uint8_t setup = 0x80 | ((disp->settings & 0x70) >> 4);

    i2c_writeBuffer(disp->address, b);
    i2c_writeByte(disp->address, brightness);
    i2c_writeByte(disp->address, setup);

    free(b.bytes);
}


void disp_init(Display* disp)
{
    disp_enable(disp);

    disp_setSettings(disp, DISP_ON, 15);
    disp_setColon(disp, false);
    disp_blank(disp);

    disp_update(disp);
}
