#include <stdlib.h>

#include <util/twi.h>

#include "i2c.h"

// uint8_t TWIGetStatus(void)
// {
//     uint8_t status;
//     //mask status
//     status = TWSR & 0xF8;
//     return status;
// }

// helpers

void start(void)
{
    TWCR |= _BV(TWINT)|_BV(TWSTA);
    while ((TWCR & _BV(TWINT)) == 0);
}

void restart(void)
{
    TWCR |= _BV(TWINT)|_BV(TWSTA)|_BV(TWSTO);
    while ((TWCR & _BV(TWINT)) == 0);
}

void stop(void)
{
    TWCR |= _BV(TWINT)|_BV(TWSTO);
}

void write(uint8_t u8data)
{
    TWDR = u8data;
    TWCR = _BV(TWINT)|_BV(TWEN);
    while ((TWCR & _BV(TWINT)) == 0);
}

uint8_t read(bool ack)
{
    if (ack)
        TWCR = _BV(TWINT)|_BV(TWEN)|_BV(TWEA);
    else
        TWCR = _BV(TWINT)|_BV(TWEN);

    while ((TWCR & _BV(TWINT)) == 0);
    return TWDR;
}

// api implementation

void i2c_init(int bitrate)
{
    // prescaler
    TWSR = 0x00;
    // set rate
    TWBR = ((F_CPU / bitrate) - 16) / (2 * 1); // 1 = 4 ^ prescaler val
    //enable TWI
    TWCR = _BV(TWEN);
}

void i2c_writeByte(uint8_t address, uint8_t byte)
{
    start();
    write((address << 1) | 0x00);
    write(byte);
    stop();
}

void i2c_writeBuffer(uint8_t address, Buffer buf)
{
    start();
    write((address << 1) | 0x00);

    uint8_t* i;

    for(i = buf.bytes; i < buf.bytes + buf.length; i++)
        write(*i);

    stop();
}

uint8_t i2c_readByte(bool ack)
{
    start();
    uint8_t byte = read(ack);
    stop();
    return byte;
}

void i2c_readBuffer(Buffer buf)
{
    start();
    for(uint8_t* i = buf.bytes; i < buf.bytes + buf.length; i++)
        *(buf.bytes) = read(i < buf.bytes + buf.length - 1 ? true : false);
    stop();
}
