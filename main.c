#include <avr/io.h>
#include <util/delay.h>

#include "i2c.h"
#include "segmented.h"

int main()
{
    DDRC |= _BV(DDC7); // LED output
    DDRB &= ~_BV(DDB5); // pin 4 input (pull-down button)

    i2c_init(9600);

    unsigned int clicks = 0;

    uint8_t prevState = 0;
    uint8_t newState = 0;

    uint8_t isRise = 0, isFall = 0;

    Display display;
    display.address = 0x70;
    disp_init(&display);

    _delay_ms(1000);

    // number test
    disp_setDigit(&display, 0, 0);
    disp_setDigit(&display, 1, 1);
    disp_setDigit(&display, 2, 2);
    disp_setDigit(&display, 3, 3);
    disp_update(&display);

    _delay_ms(1000);

    // character test
    disp_setChar(&display, 0, 't');
    disp_setChar(&display, 1, 'e');
    disp_setChar(&display, 2, 's');
    disp_setChar(&display, 3, 't');
    disp_update(&display);

    _delay_ms(1000);

    if (!disp_printUInt(&display, clicks, HEX, false))
        disp_printString(&display, "Err");

    disp_update(&display);

    while(1)
    {
        prevState = newState;
        newState = (PINB & _BV(PINB5)) >> PINB5;

        isRise = !prevState && newState;
        isFall = prevState && !newState;

        if (isRise)
        {
            PORTC |= _BV(PORTC7);   // Turn the LED on.
            clicks++;

            if (!disp_printUInt(&display, clicks, DEC, false))
                disp_printString(&display, "Err");

            disp_update(&display);
        }
        else if (isFall)
            PORTC &= !_BV(PORTC7);  // Turn the LED off.
    }
}
