#include <stdint.h>
#include <stdlib.h>

#include "bool.h"

#ifndef SEGMENTED_H
#define SEGMENTED_H

#define DISP_OFF 0x00
#define DISP_ON 0x01
#define DISP_BLINK_2HZ 0x03
#define DISP_BLINK_1HZ 0x05
#define DISP_BLINK_HALF_HZ 0x07

#define BUFFER_LEN 16

typedef struct Display {
    uint8_t address;
    uint8_t settings;
    uint8_t buffer[BUFFER_LEN];
} Display;

void disp_init(Display* disp);

void disp_enable(Display* disp);
void disp_disable(Display* disp);

void disp_blank(Display* disp);

void disp_setSettings(Display* disp, uint8_t dispSetting, uint8_t brightness);

bool disp_setDigit(Display* disp, uint8_t position, uint8_t value);
bool disp_setChar(Display* disp, uint8_t position, char value);
void disp_setGlyph(Display* disp, uint8_t position, uint8_t glyph);

bool disp_setPeriod(Display* disp, uint8_t position, bool on);

void disp_setColon(Display* disp, bool on);

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2

bool disp_printUInt(Display* disp, uint16_t value, uint16_t base, bool isNegative);
bool disp_printInt(Display* disp, int16_t value, uint16_t base);

bool disp_printString(Display* disp, char* string);

void disp_update(Display* disp);

#endif
